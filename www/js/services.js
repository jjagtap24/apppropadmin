angular.module('your_app_name.services', [])

.service('FeedList', function ($rootScope, FeedLoader, $q){
	this.get = function(feedSourceUrl) {
		var response = $q.defer();
		//num is the number of results to pull form the source
		FeedLoader.fetch({q: feedSourceUrl, num: 20}, {}, function (data){
			response.resolve(data.responseData);
		});
		return response.promise;
	};
})


// PUSH NOTIFICATIONS
.service('PushNotificationsService', function ($rootScope, $cordovaPush, NodePushServer, GCM_SENDER_ID){
	/* Apple recommends you register your application for push notifications on the device every time it’s run since tokens can change. The documentation says: ‘By requesting the device token and passing it to the provider every time your application launches, you help to ensure that the provider has the current token for the device. If a user restores a backup to a device other than the one that the backup was created for (for example, the user migrates data to a new device), he or she must launch the application at least once for it to receive notifications again. If the user restores backup data to a new device or reinstalls the operating system, the device token changes. Moreover, never cache a device token and give that to your provider; always get the token from the system whenever you need it.’ */
	this.register = function() {
		var config = {};

		// ANDROID PUSH NOTIFICATIONS
		if(ionic.Platform.isAndroid())
		{
			config = {
				"senderID": GCM_SENDER_ID
			};

			$cordovaPush.register(config).then(function(result) {
				// Success
				console.log("$cordovaPush.register Success");
				console.log(result);
			}, function(err) {
				// Error
				console.log("$cordovaPush.register Error");
				console.log(err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(JSON.stringify([notification]));
				switch(notification.event)
				{
					case 'registered':
						if (notification.regid.length > 0 ) {
							console.log('registration ID = ' + notification.regid);
							NodePushServer.storeDeviceToken("android", notification.regid);
						}
						break;

					case 'message':
						if(notification.foreground == "1")
						{
							console.log("Notification received when app was opened (foreground = true)");
						}
						else
						{
							if(notification.coldstart == "1")
							{
								console.log("Notification received when app was closed (not even in background, foreground = false, coldstart = true)");
							}
							else
							{
								console.log("Notification received when app was in background (started but not focused, foreground = false, coldstart = false)");
							}
						}

						// this is the actual push notification. its format depends on the data model from the push server
						console.log('message = ' + notification.message);
						break;

					case 'error':
						console.log('GCM error = ' + notification.msg);
						break;

					default:
						console.log('An unknown GCM event has occurred');
						break;
				}
			});

			// WARNING: dangerous to unregister (results in loss of tokenID)
			// $cordovaPush.unregister(options).then(function(result) {
			//   // Success!
			// }, function(err) {
			//   // Error
			// });
		}

		if(ionic.Platform.isIOS())
		{
			config = {
				"badge": true,
				"sound": true,
				"alert": true
			};

			$cordovaPush.register(config).then(function(result) {
				// Success -- send deviceToken to server, and store for future use
				console.log("result: " + result);
				NodePushServer.storeDeviceToken("ios", result);
			}, function(err) {
				console.log("Registration error: " + err);
			});

			$rootScope.$on('$cordovaPush:notificationReceived', function(event, notification) {
				console.log(notification.alert, "Push Notification Received");
			});
		}
	};
})


// BOOKMARKS FUNCTIONS
.service('BookMarkService', function (_, $rootScope){

	this.bookmarkFeedPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_feed_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_feed_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.link == bookmark_post.link; });

		if(!existing_post){
			user_bookmarks.push({
				link: bookmark_post.link,
				title : bookmark_post.title,
				date: bookmark_post.publishedDate,
				excerpt: bookmark_post.contentSnippet
			});
		}

		window.localStorage.ionFullApp_feed_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.bookmarkWordpressPost = function(bookmark_post){

		var user_bookmarks = !_.isUndefined(window.localStorage.ionFullApp_wordpress_bookmarks) ?
														JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks) : [];

		//check if this post is already saved

		var existing_post = _.find(user_bookmarks, function(post){ return post.id == bookmark_post.id; });

		if(!existing_post){
			user_bookmarks.push({
				id: bookmark_post.id,
				title : bookmark_post.title,
				date: bookmark_post.date,
				excerpt: bookmark_post.excerpt
			});
		}

		window.localStorage.ionFullApp_wordpress_bookmarks = JSON.stringify(user_bookmarks);
		$rootScope.$broadcast("new-bookmark");
	};

	this.getBookmarks = function(){
		return {
			feeds : JSON.parse(window.localStorage.ionFullApp_feed_bookmarks || '[]'),
			wordpress: JSON.parse(window.localStorage.ionFullApp_wordpress_bookmarks || '[]')
		};
	};
})


// WP POSTS RELATED FUNCTIONS
.service('PostService', function ($rootScope, $http, $q, WORDPRESS_API_URL){

	this.getRecentPosts = function(page) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_recent_posts/' +
		'?page='+ page +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};


	this.getPost = function(postId) {
		var deferred = $q.defer();

		$http.jsonp(WORDPRESS_API_URL + 'get_post/' +
		'?post_id='+ postId +
		'&callback=JSON_CALLBACK')
		.success(function(data) {
			deferred.resolve(data);
		})
		.error(function(data) {
			deferred.reject(data);
		});

		return deferred.promise;
	};

	this.shortenPosts = function(posts) {
		//we will shorten the post
		//define the max length (characters) of your post content
		var maxLength = 500;
		return _.map(posts, function(post){
			if(post.content.length > maxLength){
				//trim the string to the maximum length
				var trimmedString = post.content.substr(0, maxLength);
				//re-trim if we are in the middle of a word
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf("</p>")));
				post.content = trimmedString;
			}
			return post;
		});
	};

	this.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};

})

.service('APIInterceptor', function($rootScope, $injector) {
  var service = this;
  service.request = function(config) {
  	if (config.url.substr(config.url.length - 5) == '.html') {
    } else {
      $injector.get("$ionicLoading").show({
        template:'Loading....'
      });
    }
    return config;
  };

  service.response = function(response) {
    if(response.config.url.substr(response.config.url.length - 5) == '.html'){
    }else {
      $injector.get("$ionicLoading").hide();
    }
    return response;
  };

  service.responseError = function(response) {
    var popupParams={};
    $injector.get("$ionicLoading").hide();
    if(response.config.url.substr(response.config.url.length - 5) == '.html'){
      // console.log(config.url);
      return response;
    }else if(response.config.url.indexOf(baseURL + "auth/") >= 0){
      return response;
    }

    if(response.status === 500){
      popupParams = {template: "Please try Again Later after sometime."};
    }else if(response.status === 404){
      popupParams = {template: "Data not found, please try Again Later."};
    }else if (response.status === 401) {
      popupParams.template = "You are not authorized user. Please login again.s";
    }else if (response.status === 403) {
      popupParams.template = "Invalid option selected. Please try Again Later.";
    }else if (response.status === 422) {
      popupParams.template = "Bad Request. Please try Again Later.";
    }else {
      return response;
    }
    $injector.get('popup').getAlertPopup(popupParams);
  };
})

.service('AuthService', function (server_url,$rootScope, $http, $q,$timeout,$window,$stateParams,$filter){
	this.GetLoginDetail =function (username,encodedString)
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetLoginDetails/'+ angular.toJson(username) +'/'+ angular.toJson(encodedString),
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	this.GetSignUpDetail =function (signupuser)
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'POST',
             url: server_url+'/InsertSignup',
             headers: {'Content-Type': 'application/json', data:angular.toJson(signupuser)}
      	}).success(function (res)
		   {
		   	deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
	this.GetallLeads =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllLeads',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var allleads=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  this.GetallFollowup =function (leadId)
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetFollowupDetails/'+ leadId,
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var alllfollowups=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  this.InsertLeadDetail =function (insertleaddata)
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'POST',
             url: server_url+'/insertLead',
             headers: {'Content-Type': 'application/json', data:angular.toJson(insertleaddata)}
      	}).success(function (res)
		   {
		   var insertleaddata=angular.toJson(res);
		   deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  var GetAllLeads = {};
 	  this.fetchLeadSpecificData=function(leadId,GetAllLeads){
                
                if(!GetAllLeads || GetAllLeads==[]) { GetAllLeads = angular.fromJson(sessionStorage.allleads);
                }
                if(GetAllLeads){
                     
                var single_object = $filter('filter')(GetAllLeads, function (d) {return d.ID == leadId;})[0];
                //alert(angular.toJson(single_object));
                selectedData=single_object;
                
                //alert(angular.toJson(GetAllLeads));
                return single_object;
            }
             };
      this.EditLeadDetail =function (editleaddata)
      {
      	var deferred = $q.defer();
     	$http
		({
		 method  : 'POST',
         url: server_url+'/UpdateEditLead',
         headers: {'Content-Type': 'application/json', data:angular.toJson(editleaddata)}
      	}).success(function (res)
		   {
		   var editleaddata=angular.toJson(res);
		   deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
 	  this.InsertFollowUp =function (followup)
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'POST',
             url: server_url+'/InsertFollowup',
             headers: {'Content-Type': 'application/json', data:angular.toJson(followup)},
      	}).success(function (res)
		   {
		   var followup=angular.toJson(res);
		   deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
 	  this.GetallProperty =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllProprety',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var allproperty=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  this.GetallProject =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllProject',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var allproject=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  this.GetFUReminder =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllFollowupreminder',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var fpreminder=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
 	  this.GetFUPending =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/getPendingleads',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var fppending=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
 	  this.GetallStaff =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllStaff',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var allstaff=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };

 	  this.GetallOwner =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetAllOwner',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var allowner=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };
 	  this.GetallTenant =function ()
      {
      	var deferred = $q.defer();
     	$http
		({
			 method  : 'GET',
             url: server_url+'/GetTenantList',
             headers: {'Content-Type': 'application/json'},
      	}).success(function (res)
		   {
		   var alltenant=angular.toJson(res);
		    deferred.resolve(res);
		  }).error( function(data)
		  {
		   deferred.reject(data);
		  });
    		return deferred.promise;
 	  };


});