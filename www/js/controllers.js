angular.module('your_app_name.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig) {

})

//LOGIN
.controller('LoginCtrl', function($scope,AuthService,server_url,$state, $templateCache, $q, $rootScope,$http,$ionicPopup) {
	$scope.user = {};
	$scope.selected_tab = "";
	$scope.$on('my-tabs-changed', function (event, data) {
		$scope.selected_tab = data.title;
	});
	
	$scope.DoLogin = function(username,password)
	{		
	  	var updata=angular.copy($scope.user);
		var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
		var encodedString = Base64.encode(updata.password);
		var decodedString = Base64.decode(encodedString);
	    AuthService.GetLoginDetail(username,encodedString).then(function(res)
	    { 
	       if (res) 
		  {
		    $ionicPopup.alert({title: 'Success',template:"Username & Password is Correct",text:'Ok'});
		       $state.go('app.feeds-categories');
		  }else
		  {
		    $ionicPopup.alert({title: 'Error',template:"Username & Password is Incorrect please try again",text:'Ok' });
		  }
	    },function (data) 
	       {
	        $ionicPopup.alert({title: 'Error',template:"Please try again" + data,text:'Ok'});
	       });
	};
})

/*.controller('logoutctrl', function($scope,$state,$window,AuthService, $timeout,  _) {
  var logout=function() {AuthService.ClearCredentials();
  localStorage.removeItem('currentUser');
   localStorage.removeItem('locallogindata');
   localStorage.removeItem('localprofilepic');
   localStorage.removeItem('alldata');
  $state.go('auth.walkthrough');
};
$timeout(function () {logout();},100);
})*/

.controller('SignupCtrl', function($scope, $state,$http,$ionicPopup,$q,server_url,AuthService) {
	$scope.signupuser = {};
	$scope.DoSignUp = function()
	{		
	    var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
		var ENcodedString = Base64.encode($scope.signupuser.passMD5);
		$scope.signupuser.passMD5 = ENcodedString;
		AuthService.GetSignUpDetail($scope.signupuser).then(function(res)
	    { 
	       if (res) 
		  {
		    $ionicPopup.alert({title: 'Success',template:"Registration Successful Please Login",text:'Ok'});
		    $state.go('app.feeds-categories');
		  }else
		  {
		    $ionicPopup.alert({title: 'Error',template:"Registration Unsuccessful",text:'Ok' });
		  }
	    },function (data) 
	       {
	        $ionicPopup.alert({title: 'Error',template:"Please try again" + data,text:'Ok'});
	       });
	};
})

.controller('AddLeadCtrl', function($scope, $state,$http,$ionicPopup,$q,server_url,AuthService,ionicTimePicker) {
	$scope.insertleaddata = {};
	$scope.leadtime = moment(new Date()).format('hh:mm A');
    $scope.insertleaddata.followuptime= $scope.leadtime;
	$scope.InsertLead = function()
	{		
	    AuthService.InsertLeadDetail($scope.insertleaddata).then(function(res)
	    { 
	       if (res) 
		  {
		    $ionicPopup.alert({title: 'Success',template:"Lead added successfully",text:'Ok'});
		    $state.go('app.feeds-categories');
		  }else
		  {
		    $ionicPopup.alert({title: 'Error',template:"Please try again",text:'Ok' });
		  }
	    },function (data) 
	       {
	        $ionicPopup.alert({title: 'Error',template:"Please try again" + data,text:'Ok'});
	       });
	};
	// addlead datepicker
  	$scope.datepickerObject = 
  	 {
	  titleLabel: 'Date', 
	  todayLabel: 'Today', 
	  closeLabel: 'Close', 
	  setLabel: 'Ok', 
	  inputDate: new Date(), 
	  mondayFirst: true, 
	  templateType: 'popup',
	  showTodayButton: 'true',
	  from: new Date(2012, 8, 2),
	  to: new Date(2020, 8, 25), 
	  callback: function (val) {datePickerCallback(val);},
	  dateFormat: 'dd-MM-yyyy',
	  closeOnSelect: false,
	 };
	var datePickerCallback = function (val) {
		if (typeof(val) === 'undefined') {
			} else {
		    $scope.insertleaddata.followupdate = new Date(val);
		    $scope.datepickerObject.inputDate = moment(new Date(val)).format('DD-MM-YYYY');
		   }
	};
	// addlead timepicker
	 var timepickerobj = {
	    callback: function (val) {     
	      if (typeof (val) === 'undefined') {
	      } else {
	      	$scope.selectedTime = new Date(val*1000);
	      	$scope.hours= $scope.selectedTime.getUTCHours();
	        $scope.minutes= $scope.selectedTime.getUTCMinutes();
	        $scope.meridiem = $scope.hour < 12 ? "AM" : "PM";
	        $scope.insertleaddata.followuptime = $scope.hours + ':' + $scope.minutes+ ' ' + $scope.meridiem;
	        }
	    },
	};
	$scope.opentime=function(){
	  ionicTimePicker.openTimePicker(timepickerobj);
	};
})

.controller('EditLeadCtrl', function($scope,$stateParams,$filter, $state,$http,$ionicPopup,$q,server_url,AuthService,ionicTimePicker) {
	var leadId = $stateParams.ID;
	$scope.leadDetails=AuthService.fetchLeadSpecificData(leadId);
	$scope.editleaddata=$scope.leadDetails;
	delete $scope.editleaddata.next_followup_date;
  	delete $scope.editleaddata.next_followup_time;
  	delete $scope.editleaddata.date;
  	$scope.EditLead = function()
	{		
	    AuthService.EditLeadDetail($scope.editleaddata).then(function(res)
	    { 
	       if (res) 
		  {
		    $ionicPopup.alert({title: 'Success',template:"Lead added successfully",text:'Ok'});
		    $state.go('app.feeds-categories');
		  }else
		  {
		    $ionicPopup.alert({title: 'Error',template:"Please try again",text:'Ok' });
		  }
	    },function (data) 
	       {
	        $ionicPopup.alert({title: 'Error',template:"Please try again" + data,text:'Ok'});
	       });
	};
	// editlead datepicker
  	$scope.datepickerObject = 
  	 {
	  titleLabel: 'Date', 
	  todayLabel: 'Today', 
	  closeLabel: 'Close', 
	  setLabel: 'Ok', 
	  inputDate: new Date(), 
	  mondayFirst: true, 
	  templateType: 'popup',
	  showTodayButton: 'true',
	  from: new Date(2012, 8, 2),
	  to: new Date(2020, 8, 25), 
	  callback: function (val) {datePickerCallback(val);},
	  dateFormat: 'dd-MM-yyyy',
	  closeOnSelect: false,
	 };
	var datePickerCallback = function (val) {
		if (typeof(val) === 'undefined') {
			} else {
		    $scope.editleaddata.req_start_date = new Date(val);
		    $scope.datepickerObject.inputDate = moment(new Date(val)).format('DD-MM-YYYY');
		   }
	};
})

.controller('LeadCtrl', function($scope,server_url,$state,AuthService,$templateCache, $q, $rootScope,$http,$ionicPopup) {
	$scope.FetchLeads = function()
	  {
	      AuthService.GetallLeads().then(function(response)
	        { 
	            $scope.allleads=response;
	            sessionStorage.allleads =angular.toJson($scope.allleads);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchLeads();

  	$scope.DetailLead=function(getallleaddata)
  	{
	    $rootScope.getallleadid=getallleaddata.ID;
	    $state.go('app.leaddetails',{'ID' : getallleaddata.ID});
   	};
})

.controller('LeadDetailCtrl', function($scope, $state,$window,$rootScope) {
	$scope.detailleaddata=(angular.fromJson(sessionStorage.allleads));
	var id = $rootScope.getallleadid;
	$scope.detailleaddata.forEach( function(entry) 
	{
		if (entry.ID==id){ $scope.detailallleadlist=entry; }
 	});
})

.controller('FollowUpCtrl', function($window,$scope,$stateParams,$filter,AuthService,ionicTimePicker ,$state,$http,$ionicPopup,$q,server_url) {
	$scope.followup={};
	$scope.numLimit = 10;
	var leadId = $stateParams.ID;
  	$scope.followup={lead_id :leadId};
  	$scope.folltime = moment(new Date()).format('hh:mm A');
    $scope.followup.next_followup_time= $scope.folltime;
  	$scope.FollowUp = function()
  	{
  		AuthService.InsertFollowUp($scope.followup).then(function(d)
	      {
	      	$scope.FetchFollowUp(leadId);
	      	$ionicPopup.alert({title:'Success',template:"Data Inserted Successfully",text:'Ok'});
	      },function(errResponse)
	      {
	      	$ionicPopup.alert({title:'Error',template:"Please try again",text:'Ok'});
	        console.error('Error'+(angular.toJson(errResponse)));
	      });
    };
    // followup datepicker
  	$scope.datepickerObject = 
  	 {
	  titleLabel: 'Date', 
	  todayLabel: 'Today', 
	  closeLabel: 'Close', 
	  setLabel: 'Ok', 
	  inputDate: new Date(), 
	  mondayFirst: true, 
	  templateType: 'popup',
	  showTodayButton: 'true',
	  from: new Date(2012, 8, 2),
	  to: new Date(2020, 8, 25), 
	  callback: function (val) {  datePickerCallback(val); },
	  dateFormat: 'dd-MM-yyyy',
	  closeOnSelect: false,
	 };
	var datePickerCallback = function (val) {
		if (typeof(val) === 'undefined') {
			} else {
		    $scope.followup.next_followup_date = new Date(val);
		    $scope.datepickerObject.inputDate = moment(new Date(val)).format('DD-MM-YYYY');
		   }
	};
	// followup timepicker
	var timepickerobj = {
	    callback: function (val) {     
	      if (typeof (val) === 'undefined') {
	      } else {
	      	$scope.selectedTime = new Date(val*1000);
	      	$scope.hours= $scope.selectedTime.getUTCHours();
	        $scope.minutes= $scope.selectedTime.getUTCMinutes();
	        $scope.meridiem = $scope.hour < 12 ? "AM" : "PM";
	        $scope.followup.next_followup_time = $scope.hours + ':' + $scope.minutes+ ' ' + $scope.meridiem;
	        }
	    },
	};
	$scope.opentime=function(){
	  ionicTimePicker.openTimePicker(timepickerobj);
	};

	$scope.FetchFollowUp = function()
	  {
	      AuthService.GetallFollowup(leadId).then(function(response)
	        { 
	            $scope.alllfollowups=response;
	            sessionStorage.alllfollowups =angular.toJson($scope.alllfollowups);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	           });
	  };

	   $scope.showFpRemark = function() {
	   var fpremark = angular.fromJson($window.sessionStorage.alllfollowups)[0].details;
	  	var alertPopup = $ionicPopup.alert({
	     title: 'FollowUp Remark',
	     template: fpremark
	   });
	  	alertPopup.then(function(res) {
	     });
	 };
	$scope.FetchFollowUp();
})

.controller('PropertyListCtrl', function($scope,server_url,$state,AuthService,$templateCache, $q, $rootScope,$http,$ionicPopup) {
	$scope.FetchProperty = function()
	  {
	      AuthService.GetallProperty().then(function(response)
	        { 
	            $scope.allproperty=response;
	            sessionStorage.allproperty =angular.toJson($scope.allproperty);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchProperty();
	$scope.DetailProperty=function(getallpropertydata)
  	{
	    $rootScope.getallpropertyid=getallpropertydata.property_id;
	    $state.go('app.propertydetails',{'property_id' : getallpropertydata.property_id});
   	};
})

.controller('PropertyDetailCtrl', function($scope, $state,$window,$rootScope) {
	$scope.detailpropertydata=(angular.fromJson(sessionStorage.allproperty));
	var id = $rootScope.getallpropertyid;
	$scope.detailpropertydata.forEach( function(entry) 
	{
		if (entry.property_id==id){ $scope.detailallpropertylist=entry; }
 	});
})

.controller('ProjectListCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.ProjenmLimit = 1;
	$scope.FetchProject = function()
	  {
	      AuthService.GetallProject().then(function(response)
	        { 
	            $scope.allproject=response;
	            sessionStorage.allproject =angular.toJson($scope.allproject);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchProject();
	$scope.DetailProject=function(getallprojectdata)
  	{
	    $rootScope.getallprojectid=getallprojectdata.ID;
	    $state.go('app.projectdetail',{'ID' : getallprojectdata.ID});
   	};
})

.controller('ProjectDetailCtrl', function($scope, $state,$window,$rootScope) {
	$scope.detailprojectdata=(angular.fromJson(sessionStorage.allproject));
	var id = $rootScope.getallprojectid;
	$scope.detailprojectdata.forEach( function(entry) 
	{
		if (entry.ID==id){ $scope.detailallprojectlist=entry; }
 	});
})

.controller('FollowupReminderCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.FetchFolloupRem = function()
	  {
	      AuthService.GetFUReminder().then(function(response)
	        { 
	            $scope.fpreminder=response;
	            sessionStorage.fpreminder =angular.toJson($scope.fpreminder);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	  $scope.FetchFolloupRem();
})

.controller('FollowupPendingCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.FetchFolloupPending = function()
	  {
	      AuthService.GetFUPending().then(function(response)
	        { 
	            $scope.fppending=response;
	            sessionStorage.fppending =angular.toJson($scope.fppending);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchFolloupPending();
})

.controller('StaffListCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.FetchStaff = function()
	  {
	      AuthService.GetallStaff().then(function(response)
	        { 
	            $scope.allstaff=response;
	            sessionStorage.allstaff =angular.toJson($scope.allstaff);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchStaff();
})
.controller('OwnerListCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.FetchOwner = function()
	  {
	      AuthService.GetallOwner().then(function(response)
	        { 
	            $scope.allowner=response;
	            sessionStorage.allowner =angular.toJson($scope.allowner);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchOwner();
})

.controller('TenantListCtrl', function($scope,$state,AuthService,$templateCache, $q, $rootScope) {
	$scope.FetchTenant = function()
	  {
	      AuthService.GetallTenant().then(function(response)
	        { 
	            $scope.alltenant=response;
	            sessionStorage.alltenant =angular.toJson($scope.alltenant);
		        $scope.$broadcast('scroll.refreshComplete');
		        return response;
	        }, function (errResponse) 
	           {
                $scope.$broadcast('scroll.refreshComplete');
	            return $q.reject(errResponse);
	            });
	  };
	$scope.FetchTenant();
})

/*.controller('ForgotPasswordCtrl', function($scope, $state) {
	$scope.recoverPassword = function(){
		$state.go('app.feeds-categories');
	};

	$scope.user = {};
})*/

/*.controller('RateApp', function($scope) {
	$scope.rateApp = function(){
		if(ionic.Platform.isIOS()){
			//you need to set your own ios app id
			AppRate.preferences.storeAppURL.ios = '1234555553>';
			AppRate.promptForRating(true);
		}else if(ionic.Platform.isAndroid()){
			//you need to set your own android app id
			AppRate.preferences.storeAppURL.android = 'market://details?id=ionFB';
			AppRate.promptForRating(true);
		}
	};
})*/

/*.controller('SendMailCtrl', function($scope) {
	$scope.sendMail = function(){
		cordova.plugins.email.isAvailable(
			function (isAvailable) {
				// alert('Service is not available') unless isAvailable;
				cordova.plugins.email.open({
					to:      'envato@startapplabs.com',
					cc:      'hello@startapplabs.com',
					// bcc:     ['john@doe.com', 'jane@doe.com'],
					subject: 'Greetings',
					body:    'How are you? Nice greetings from IonFullApp'
				});
			}
		);
	};
})*/

/*.controller('MapsCtrl', function($scope, $ionicLoading) {

	$scope.info_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.center_position = {
		lat: 43.07493,
		lng: -89.381388
	};

	$scope.my_location = "";

	$scope.$on('mapInitialized', function(event, map) {
		$scope.map = map;
	});

	$scope.centerOnMe= function(){

		$scope.positions = [];

		$ionicLoading.show({
			template: 'Loading...'
		});

		// with this function you can get the user’s current position
		// we use this plugin: https://github.com/apache/cordova-plugin-geolocation/
		navigator.geolocation.getCurrentPosition(function(position) {
			var pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			$scope.current_position = {lat: pos.G,lng: pos.K};
			$scope.my_location = pos.G+", "+pos.K;
			$scope.map.setCenter(pos);
			$ionicLoading.hide();
		});
	};
})*/

/*.controller('AdsCtrl', function($scope, $ionicActionSheet, AdMob, iAd) {

	$scope.manageAdMob = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
				{ text: 'Show Banner' },
				{ text: 'Show Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				AdMob.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show Banner')
				{
					console.log("show banner");
					AdMob.showBanner();
				}

				if(button.text == 'Show Interstitial')
				{
					console.log("show interstitial");
					AdMob.showInterstitial();
				}

				return true;
			}
		});
	};

	$scope.manageiAd = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			buttons: [
			{ text: 'Show iAd Banner' },
			{ text: 'Show iAd Interstitial' }
			],
			destructiveText: 'Remove Ads',
			titleText: 'Choose the ad to show - Interstitial only works in iPad',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			destructiveButtonClicked: function() {
				console.log("removing ads");
				iAd.removeAds();
				return true;
			},
			buttonClicked: function(index, button) {
				if(button.text == 'Show iAd Banner')
				{
					console.log("show iAd banner");
					iAd.showBanner();
				}
				if(button.text == 'Show iAd Interstitial')
				{
					console.log("show iAd interstitial");
					iAd.showInterstitial();
				}
				return true;
			}
		});
	};
})*/

// FEED
//brings all feed categories
.controller('FeedsCategoriesCtrl', function($scope, $http) {
	$scope.feeds_categories = [];

	$http.get('feeds-categories.json').success(function(response) {
		$scope.feeds_categories = response;
	});
})

//bring specific category providers
.controller('CategoryFeedsCtrl', function($scope, $http, $stateParams) {
	$scope.category_sources = [];

	$scope.categoryId = $stateParams.categoryId;

	$http.get('feeds-categories.json').success(function(response) {
		var category = _.find(response, {id: $scope.categoryId});
		$scope.categoryTitle = category.title;
		$scope.category_sources = category.feed_sources;
	});
})

//this method brings posts for a source provider
.controller('FeedEntriesCtrl', function($scope, $stateParams, $http, FeedList, $q, $ionicLoading, BookMarkService) {
	$scope.feed = [];

	var categoryId = $stateParams.categoryId,
			sourceId = $stateParams.sourceId;

	$scope.doRefresh = function() {

		$http.get('feeds-categories.json').success(function(response) {

			$ionicLoading.show({
				template: 'Loading entries...'
			});

			var category = _.find(response, {id: categoryId }),
					source = _.find(category.feed_sources, {id: sourceId });

			$scope.sourceTitle = source.title;

			FeedList.get(source.url)
			.then(function (result) {
				$scope.feed = result.feed;
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			}, function (reason) {
				$ionicLoading.hide();
				$scope.$broadcast('scroll.refreshComplete');
			});
		});
	};

	$scope.doRefresh();

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkFeedPost(post);
	};
})

// SETTINGS
.controller('SettingsCtrl', function($scope, $ionicActionSheet, $state) {
	$scope.airplaneMode = true;
	$scope.wifi = false;
	$scope.bluetooth = true;
	$scope.personalHotspot = true;

	$scope.checkOpt1 = true;
	$scope.checkOpt2 = true;
	$scope.checkOpt3 = false;

	$scope.radioChoice = 'B';

	// Triggered on a the logOut button click
	$scope.showLogOutMenu = function() {

		// Show the action sheet
		var hideSheet = $ionicActionSheet.show({
			//Here you can add some more buttons
			// buttons: [
			// { text: '<b>Share</b> This' },
			// { text: 'Move' }
			// ],
			destructiveText: 'Logout',
			titleText: 'Are you sure you want to logout? This app is awsome so I recommend you to stay.',
			cancelText: 'Cancel',
			cancel: function() {
				// add cancel code..
			},
			buttonClicked: function(index) {
				//Called when one of the non-destructive buttons is clicked,
				//with the index of the button that was clicked and the button object.
				//Return true to close the action sheet, or false to keep it opened.
				return true;
			},
			destructiveButtonClicked: function(){
				//Called when the destructive button is clicked.
				//Return true to close the action sheet, or false to keep it opened.
				$state.go('auth.walkthrough');
			}
		});

	};
})

// TINDER CARDS
.controller('TinderCardsCtrl', function($scope, $http) {

	$scope.cards = [];


	$scope.addCard = function(img, name) {
		var newCard = {image: img, name: name};
		newCard.id = Math.random();
		$scope.cards.unshift(angular.extend({}, newCard));
	};

	$scope.addCards = function(count) {
		$http.get('http://api.randomuser.me/?results=' + count).then(function(value) {
			angular.forEach(value.data.results, function (v) {
				$scope.addCard(v.user.picture.large, v.user.name.first + " " + v.user.name.last);
			});
		});
	};

	$scope.addFirstCards = function() {
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/left.png","Nope");
		$scope.addCard("https://dl.dropboxusercontent.com/u/30675090/envato/tinder-cards/right.png", "Yes");
	};

	$scope.addFirstCards();
	$scope.addCards(5);

	$scope.cardDestroyed = function(index) {
		$scope.cards.splice(index, 1);
		$scope.addCards(1);
	};

	$scope.transitionOut = function(card) {
		console.log('card transition out');
	};

	$scope.transitionRight = function(card) {
		console.log('card removed to the right');
		console.log(card);
	};

	$scope.transitionLeft = function(card) {
		console.log('card removed to the left');
		console.log(card);
	};
})


// BOOKMARKS
.controller('BookMarksCtrl', function($scope, $rootScope, BookMarkService, $state) {

	$scope.bookmarks = BookMarkService.getBookmarks();

	// When a new post is bookmarked, we should update bookmarks list
	$rootScope.$on("new-bookmark", function(event){
		$scope.bookmarks = BookMarkService.getBookmarks();
	});

	$scope.goToFeedPost = function(link){
		window.open(link, '_blank', 'location=yes');
	};
	$scope.goToWordpressPost = function(postId){
		$state.go('app.post', {postId: postId});
	};
})

// WORDPRESS
.controller('WordpressCtrl', function($scope, $http, $ionicLoading, PostService, BookMarkService) {
	$scope.posts = [];
	$scope.page = 1;
	$scope.totalPages = 1;

	$scope.doRefresh = function() {
		$ionicLoading.show({
			template: 'Loading posts...'
		});

		//Always bring me the latest posts => page=1
		PostService.getRecentPosts(1)
		.then(function(data){
			$scope.totalPages = data.pages;
			$scope.posts = PostService.shortenPosts(data.posts);

			$ionicLoading.hide();
			$scope.$broadcast('scroll.refreshComplete');
		});
	};

	$scope.loadMoreData = function(){
		$scope.page += 1;

		PostService.getRecentPosts($scope.page)
		.then(function(data){
			//We will update this value in every request because new posts can be created
			$scope.totalPages = data.pages;
			var new_posts = PostService.shortenPosts(data.posts);
			$scope.posts = $scope.posts.concat(new_posts);

			$scope.$broadcast('scroll.infiniteScrollComplete');
		});
	};

	$scope.moreDataCanBeLoaded = function(){
		return $scope.totalPages > $scope.page;
	};

	$scope.bookmarkPost = function(post){
		$ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
		BookMarkService.bookmarkWordpressPost(post);
	};

	$scope.doRefresh();
})

// WORDPRESS POST
.controller('WordpressPostCtrl', function($scope, post_data, $ionicLoading) {

	$scope.post = post_data.post;
	$ionicLoading.hide();

	$scope.sharePost = function(link){
		window.plugins.socialsharing.share('Check this post here: ', null, null, link);
	};
})


.controller('ImagePickerCtrl', function($scope, $rootScope, $cordovaCamera) {

	$scope.images = [];

	$scope.selImages = function() {

		window.imagePicker.getPictures(
			function(results) {
				for (var i = 0; i < results.length; i++) {
					console.log('Image URI: ' + results[i]);
					$scope.images.push(results[i]);
				}
				if(!$scope.$$phase) {
					$scope.$apply();
				}
			}, function (error) {
				console.log('Error: ' + error);
			}
		);
	};

	$scope.removeImage = function(image) {
		$scope.images = _.without($scope.images, image);
	};

	$scope.shareImage = function(image) {
		window.plugins.socialsharing.share(null, null, image);
	};

	$scope.shareAll = function() {
		window.plugins.socialsharing.share(null, null, $scope.images);
	};
})

;
